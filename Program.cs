﻿using System;
using System.Globalization;

namespace Desafio_LGPD
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            Cadastro dados = new Cadastro();

            dados.CabecalhoCadastro();
            Console.Write("Nome: ");
            dados.Nome = Console.ReadLine();
            dados.Nome = textInfo.ToTitleCase(dados.Nome);

            Console.Write("CPF: ");
            dados.Cpf = Console.ReadLine();

            Console.Write("Endereço: ");
            dados.Rua = Console.ReadLine();
            dados.Rua = textInfo.ToTitleCase(dados.Rua);

            Console.Write("Numero: ");
            dados.Numero = Console.ReadLine();

            Console.Write("Bairro: ");
            dados.Bairro = Console.ReadLine();
            dados.Bairro = textInfo.ToTitleCase(dados.Bairro);

            Console.WriteLine("Dados Privados? sim/não");
            dados.dadosPrivados = dados.EhDadoPrivado(Console.ReadLine().ToUpper());

            dados.ImprimirDados();

        }

    }
}
