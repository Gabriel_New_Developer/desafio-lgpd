﻿using System;

namespace Desafio_LGPD
{
    class Cadastro
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rua { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public bool dadosPrivados { get; set; }

        public void CabecalhoCadastro()
        {
            Console.WriteLine("________________________________________________________________________________________________________________________\n" +
                              "*************************************** Dados Para Cadastro Pessoa Fisica **********************************************\n" +
                              "________________________________________________________________________________________________________________________\n");
        }
        private string MascararCpf(string Cpf)
        {
            string[] CpfMascarado = Cpf.Split('.', ' ', '/', ',', '-');
            return Cpf.Replace(CpfMascarado[0], "***").Replace(CpfMascarado[2], "***").Replace(' ', '.').Replace('/', '.').Replace(',', '.').Replace('-', '.').Replace(';', '.'); ;
        }

        private string FormatarCpf(string Cpf)
        {
            string[] CpfFormatado = Cpf.Split('.', ' ', ',', '/', '-', '*', ';');
            return Cpf.Replace(' ', '.').Replace('/', '.').Replace(',', '.').Replace('-', '.').Replace('*', '.').Replace(';', '.');
        }
        public string EnderecoMascarado(string Rua)
        {
            string[] RUA = Rua.Split(' ');
            return Rua.Replace(RUA[0], "***");
        }

        public string NumeroPrivado(string Numero)
        {
            return Numero.Replace(Numero, "***");
        }

        public bool EhDadoPrivado(string dadosPrivados)
        {
            return dadosPrivados.ToLower().Equals("sim");

            // if (dadosPrivados.ToLower().Equals("sim"))
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public string RetornarDescricaoDadoPrivado(bool dadosPrivados)
        {
            return dadosPrivados.Equals(true) ? "Sim" : "Não";
        }
        public string Opcao(bool dadosPrivados)
        {
            return dadosPrivados ? "Sim" : "Não";

            //if (dadosPrivados)
            //{
            //    return "Sim";
            //}
            //else
            //{
            //    return "Não";
            //}
        }

        public void ImprimirDados()
        {
            if (dadosPrivados)
            {
                EscreveCabesalho("Dados Privados");
                Console.WriteLine($"Nome: {Nome} \nCPF: {MascararCpf(Cpf)} \nEndereço: {EnderecoMascarado(Rua)} / Numero: {NumeroPrivado(Numero)} / Bairro: {Bairro} " +
                    $"\nDados Privados: \n{Opcao(dadosPrivados)}");
                EscreverRodape();
            }
            else
            {
                EscreveCabesalho("Dados Publicos");
                Console.WriteLine($"Nome: {Nome} \nCPF: {FormatarCpf(Cpf)} \nEndereço: {Rua} / Numero: {Numero} / Bairro: {Bairro} " +
                    $"\nDados Privados? \n{Opcao(dadosPrivados)}");
                EscreverRodape();
            }
        }

        private static void EscreverRodape()
        {
            Console.WriteLine("........................................................................................................................");
        }

        private static void EscreveCabesalho(string titulo)
        {
            Console.WriteLine($"................................................... {titulo} .....................................................\n");
        }
    }
}